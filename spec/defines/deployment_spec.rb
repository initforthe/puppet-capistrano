# frozen_string_literal: true

require 'spec_helper'

describe 'capistrano::deployment' do
  let :default_params do
    {
      user: 'root',
      group: 'root',
      deploy_root: '/var/www',
      shared_dirs: [
        'log', 'tmp', 'public', 'tmp/pids', 'public/system',
        'public/assets', 'tmp/cache'
      ],
      system_dirs: {
        'log_dir'   => 'log',
        'pid_dir'   => 'tmp/pids',
        'cache_dir' => 'tmp/cache',
      },
      system_dir_map: {
        'log_dir'   => '/var/log',
        'pid_dir'   => '/var/run',
        'cache_dir' => '/var/cache',
      },
      system_dir_symlinks: false,
      dir_mode: 'a+rx,ug+ws',
    }
  end

  let(:title) { 'my_app' }

  context 'with default values for all parameters' do
    let(:params) { default_params }

    it { is_expected.to compile }
    it { is_expected.to contain_file('/var/www/my_app').with('ensure' => 'directory') }
    it { is_expected.to contain_file('/var/www/my_app/shared').with('ensure' => 'directory') }
    it { is_expected.to contain_file('/var/www/my_app/shared/log').with('ensure' => 'directory') }
    it { is_expected.to contain_file('/var/www/my_app/shared/public').with('ensure' => 'directory') }
    it { is_expected.to contain_file('/var/www/my_app/shared/public/system').with('ensure' => 'directory') }
    it { is_expected.to contain_file('/var/www/my_app/shared/tmp').with('ensure' => 'directory') }
    it { is_expected.to contain_file('/var/www/my_app/shared/tmp/pids').with('ensure' => 'directory') }
    it { is_expected.to contain_file('/var/www/my_app/shared/tmp/cache').with('ensure' => 'directory') }
  end

  context 'with shared_dirs => ["foo", "bar"]' do
    let(:params) do
      default_params.merge!(shared_dirs: ['foo', 'bar'])
    end

    it { is_expected.to contain_file('/var/www/my_app/shared/foo').with('ensure' => 'directory') }
    it { is_expected.to contain_file('/var/www/my_app/shared/bar').with('ensure' => 'directory') }
    it { is_expected.not_to contain_file('/var/www/my_app/shared/log') }
  end

  context 'with ensure => absent' do
    let(:params) do
      default_params.merge!(ensure: 'absent')
    end

    it { is_expected.to contain_file('/var/www/my_app').with('ensure' => 'absent') }
    it { is_expected.to contain_file('/var/www/my_app/shared').with('ensure' => 'absent') }
    it { is_expected.to contain_file('/var/www/my_app/shared/log').with('ensure' => 'absent') }
    it { is_expected.to contain_file('/var/www/my_app/shared/public').with('ensure' => 'absent') }
    it { is_expected.to contain_file('/var/www/my_app/shared/public/system').with('ensure' => 'absent') }
    it { is_expected.to contain_file('/var/www/my_app/shared/tmp').with('ensure' => 'absent') }
    it { is_expected.to contain_file('/var/www/my_app/shared/tmp/pids').with('ensure' => 'absent') }
    it { is_expected.to contain_file('/var/www/my_app/shared/tmp/cache').with('ensure' => 'absent') }
  end

  context 'with system_dir_symlinks => true' do
    let(:params) do
      default_params.merge!(system_dir_symlinks: true)
    end

    it {
      is_expected.to contain_file('/var/www/my_app/shared/log').with(
        'ensure' => 'link',
        'target' => '/var/log/my_app',
      )
    }
    it {
      is_expected.to contain_file('/var/www/my_app/shared/tmp/cache').with(
        'ensure' => 'link',
        'target' => '/var/cache/my_app',
      )
    }
    it {
      is_expected.to contain_file('/var/www/my_app/shared/tmp/pids').with(
        'ensure' => 'link',
        'target' => '/var/run/my_app',
      )
    }

    context 'with different system directories' do
      let(:params) do
        default_params.merge!(system_dir_symlinks: true,
                              shared_dirs: ['foo', 'bar'],
                              system_dirs: {
                                'foo_dir'   => 'foo',
                                'bar_dir'   => 'bar',
                              },
                              system_dir_map: {
                                'foo_dir'   => '/var/foo',
                                'bar_dir'   => '/var/bar',
                              })
      end

      it {
        is_expected.to contain_file('/var/www/my_app/shared/foo').with(
          'ensure' => 'link',
          'target' => '/var/foo/my_app',
        )
      }
      it {
        is_expected.to contain_file('/var/www/my_app/shared/bar').with(
          'ensure' => 'link',
          'target' => '/var/bar/my_app',
        )
      }
    end
  end
end
