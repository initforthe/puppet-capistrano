# frozen_string_literal: true

require 'spec_helper'

describe 'capistrano::shared_file' do
  let(:title) { 'config/database.yml' }

  let :params do
    {
      'app' => 'my_app',
      'content' => 'foobarbaz',
    }
  end

  context 'with a valid declaration chain' do
    let(:pre_condition) do
      <<-EOF
      include ::capistrano

      capistrano::deployment { "my_app": }
      EOF
    end

    it { is_expected.to compile }
    it {
      is_expected.to contain_file('shared-file-config/database.yml-for-my_app')
        .with(path: '/var/www/my_app/shared/config/database.yml',
              content: 'foobarbaz')
    }
  end
end
