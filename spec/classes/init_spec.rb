# frozen_string_literal: true

require 'spec_helper'
describe 'capistrano' do
  context 'with default values for all parameters' do
    it { is_expected.to contain_class('capistrano') }
    it {
      is_expected.to contain_file('/var/www')
        .with('ensure' => 'directory',
              'owner'  => 'root',
              'mode'   => 'a+rx,ug+ws')
    }
  end

  context 'with deploy_root => /srv/apps' do
    let(:params) { { deploy_root: '/srv/apps' } }

    it { is_expected.to contain_file('/srv/apps') }
  end

  context 'with user => deploy' do
    let(:params) { { user: 'deploy' } }

    it {
      is_expected.to contain_file('/var/www').with(
        'owner' => 'deploy',
      )
    }
  end

  context 'with dir_mode => a+rwx' do
    let(:params) { { dir_mode: 'a+rwx' } }

    it {
      is_expected.to contain_file('/var/www').with(
        'mode' => 'a+rwx',
      )
    }
  end
end
