# Class: capistrano
# ===========================
#
# The capistrano class sets up the defaults and ensures the `deploy_root`
# directory exists.
#
# Parameters
# ----------
#
# * `user`
# The user who will own the deployment. Defaults to `root`
#
# * `shared_dirs`
# An array of directories to be created in the shared directory
# Defaults to `['config', 'log', 'tmp', 'public', 'tmp/pids', 'public/system', 'public/assets', 'tmp/cache']`
#
# * `system_dirs`
# A hash of directories to be used for symlinking to system directories.
# Default: `{ log_dir   => 'log', pid_dir   => 'tmp/pids', cache_dir => 'tmp/cache', }`
#
# * `system_dir_map`
# A map defining where on the system path directories should be created.
# Default: `{ log_dir   => '/var/log', pid_dir   => '/var/run', cache_dir => '/var/cache', }`
#
# * `deploy_root`
# The root for all deployments. Defaults to `/var/www`
#
# * `dir_mode`
# Default chmod for directories. Defaults to `a+rx,ug+ws`
#
# * `system_dir_symlinks`
# Determins whether the logs, pids and cache are symlinked to system directories
# Defaults to false
#
#
# Examples
# --------
#
# @example
#    class { 'capistrano':
#      user        => 'deploy',
#      deploy_root => '/srv/apps',
#    }
#
# Authors
# -------
#
# Tomislav Simnett <tom@initforthe.com>
#
# Copyright
# ---------
#
# Copyright 2016 Initforthe Ltd
#
class capistrano (
  String $user                         = $capistrano::params::user,
  String $group                        = $capistrano::params::group,
  Array[String] $shared_dirs           = $capistrano::params::shared_dirs,
  Hash[String, String] $system_dirs    = $capistrano::params::system_dirs,
  Hash[String, String] $system_dir_map = $capistrano::params::system_dir_map,
  String $deploy_root                  = $capistrano::params::deploy_root,
  String $dir_mode                     = $capistrano::params::dir_mode,
  Boolean $system_dir_symlinks         = $capistrano::params::system_dir_symlinks,
) inherits capistrano::params {
  file { $deploy_root:
    ensure => directory,
    owner  => $user,
    group  => $group,
    mode   => $dir_mode,
  }
}
