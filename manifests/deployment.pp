# Defined type: capistrano::deployment
# ===========================
#
# This class sets up an app deployment space. This is a series of directories,
# either the default list provided in the main capistrano setup or a custom
# array. Shared folders for logs, sockets, pids and caches can be symlinked to
# systemwide directors such as /var/log, /var/cache and /var/run
#
# Parameters
# ----------
#
# * `user`
# The user who will own the deployment. Default: `root`
#
# * `deploy_root`
# The root for all deployments. Default: `/var/www`
#
# * `shared_dirs`
# An array of directories to be created in the shared directory
# Default: `['config', 'log', 'tmp', 'public', 'tmp/pids', 'public/system', 'public/assets', 'tmp/cache']`
#
# * `system_dirs`
# A hash of directories to be used for symlinking to system directories.
# Default: `{ log_dir   => 'log', pid_dir   => 'tmp/pids', cache_dir => 'tmp/cache', }`
#
# * `system_dir_map`
# A map defining where on the system path directories should be created.
# Default: `{ log_dir   => '/var/log', pid_dir   => '/var/run', cache_dir => '/var/cache', }`
#
# * `dir_mode`
# Default chmod for directories. Default: `a+rx,ug+ws`
#
# * `system_dir_symlinks`
# Determins whether the logs, pids and cache are symlinked to system directories
# Default: false
#
#
# Examples
# --------
#
# @example
#    include ::capistrano
#
#    capistrano::deployment { 'my_app':
#      user => 'deploy',
#    }
#
# Authors
# -------
#
# Tomislav Simnett <tom@initforthe.com>
#
# Copyright
# ---------
#
# Copyright 2016 Initforthe Ltd
#
define capistrano::deployment (
  $ensure              = present,
  $user                = $capistrano::user,
  $group               = $capistrano::group,
  $deploy_root         = $capistrano::deploy_root,
  $shared_dirs         = $capistrano::shared_dirs,
  $system_dirs         = $capistrano::system_dirs,
  $system_dir_map      = $capistrano::system_dir_map,
  $system_dir_symlinks = $capistrano::system_dir_symlinks,
  $dir_mode            = $capistrano::dir_mode,
) {
  $deploy_dir = "${deploy_root}/${title}"

  $shared_dir = "${deploy_dir}/shared"
  $release_dir = "${deploy_dir}/releases"

  if ($system_dir_symlinks) {
    $non_system_shared_dirs = delete($shared_dirs, values($system_dirs))

    $full_shared_dir_paths = delete_undef_values(
      concat(prefix($non_system_shared_dirs, "${shared_dir}/"),
        suffix(values($system_dir_map), "/${title}")
      )
    )

    $system_dir_map.each |$dir, $target| {
      $system_dir = $system_dirs[$dir]
      file { "${shared_dir}/${system_dir}":
        ensure => link,
        target => "${target}/${title}",
      }
    }
  } else {
    $full_shared_dir_paths = delete_undef_values(
      prefix($shared_dirs, "${shared_dir}/")
    )
  }

  $directory_ensure = $ensure ? {
    absent  => absent,
    default => directory,
  }

  file { [
    $deploy_dir,
    $shared_dir,
    $release_dir,
    $full_shared_dir_paths,
  ]:
    ensure => $directory_ensure,
    owner  => $user,
    group  => $group,
    mode   => $dir_mode,
  }


}
