# Defined type: capistrano::shared_file
# ===========================
#
# This define adds a shared file to the specified deployment.
#
# Parameters
# ----------
#
# * `app`
# The user who will own the deployment. Default: `root`
#
# * `content`
# The root for all deployments. Default: `/var/www`
#
#
# Examples
# --------
#
# @example
#    include ::capistrano
#    capistrano::deployment { 'my_app': }
#
#    capistrano::shared_file { 'config/database.yml':
#      app     => 'my_app',
#      content => 'foobarbaz',
#    }
#
# Authors
# -------
#
# Tomislav Simnett <tom@initforthe.com>
#
# Copyright
# ---------
#
# Copyright 2016 Initforthe Ltd
define capistrano::shared_file (
  $app,
  $content   = undef,
  $file_path = $title,
  $ensure    = present,
  $owner     = $capistrano::user,
  $group     = $capistrano::group,
) {
  $path = [ $capistrano::deploy_root, $app, 'shared' ].join('/')

  file { "shared-file-${title}-for-${app}":
    ensure  => $ensure,
    path    => "${path}/${file_path}",
    content => $content,
    owner   => $owner,
    group   => $group,
    require => Capistrano::Deployment[$app],
  }
}
