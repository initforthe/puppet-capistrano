# capistrano default params
class capistrano::params {
  $user                = 'root'
  $group               = $user
  $shared_dirs         = [
    'config',
    'log',
    'tmp',
    'public',
    'tmp/pids',
    'public/system',
    'public/assets',
    'tmp/cache',
    'node_modules',
    'storage',
    'vendor',
    'vendor/bundle',
  ]
  $system_dirs         = {
    log_dir   => 'log',
    pid_dir   => 'tmp/pids',
    cache_dir => 'tmp/cache',
  }
  $system_dir_map      = {
    log_dir   => '/var/log',
    pid_dir   => '/var/run',
    cache_dir => '/var/cache',
  }
  $deploy_root         = '/var/www'
  $dir_mode            = 'a+rx,ug+ws'
  $system_dir_symlinks = false
}
