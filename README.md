# capistrano

#### Table of Contents

1. [Description](#description)
1. [Setup - The basics of getting started with capistrano](#setup)
    * [Beginning with capistrano](#beginning-with-capistrano)
1. [Usage - Configuration options and additional functionality](#usage)
    * [Configure all capistrano deployments](#configure-all-capistrano-deployments)
1. [Reference - An under-the-hood peek at what the module is doing and how](#reference)
    * [Classes](#classes)
    * [Defined Types](#defined-types)
1. [Limitations - OS compatibility, etc.](#limitations)
1. [Development - Guide for contributing to the module](#development)

## Description

This module sets up the capistrano directory structure on a server.

If you use capistrano to deploy your apps, this module will allow you to set up
your shared folders, and symlink to system directories if desired.

## Setup

### Beginning with capistrano

Usage can be as simple as:

```
include ::capistrano
capistrano::deployment { 'my_app': }
```

This will set up a directory structure under `/var/www/my_app` as the `root`
user.

## Usage

### Configure all capistrano deployments

For default settings, declare the `capistrano` class above. To customise your
deployments at a global level, specify the parameters you want to change:

```
class { 'capistrano':
  user                => 'deploy',
  shared_dirs         => ['foo', 'bar', 'baz'],
  deploy_root         => '/srv/apps',
  dir_mode            => 'a+rwx', # DON'T DO THIS!
  system_dir_symlinks => true,
}
```

### Set up a deployment

The same parameters apply at a local level as on a global level. To use the default settings, you can define a simple `capistrano::deployment` as above. To customise it, specify the parameters you want to change:

```
capistrano::deployment { 'my_app':
  user                => 'deploy',
  shared_dirs         => ['foo', 'bar', 'baz'],
  deploy_root         => '/srv/apps',
  dir_mode            => 'a+rwx', # DON'T DO THIS!
  system_dir_symlinks => true,
}
```

### Add a shared file

A shared file can be added as needed which capistrano can then link to:

```
capistrano::shared_file { 'config/database.yml':
  app     => 'my_app',
  content => 'foobarbaz',
}
```

## Reference

**Classes:**

* [capistrano](#capistrano)

**Defined Types:**

* [capistrano::deployment](#capistrano_deployment)
* [capistrano::shared_file](#capistrano_shared_file)

### Classes

#### capistrano

Sets up the root for all capistrano deployments. Set the following parameters if you want something other than the default settings:

##### `user`

Which user you want to own the deployments. The user must exist already.

##### `shared_dirs`

An array of directories you want in your shared directory. Default: `['log', 'tmp', 'public', 'tmp/pids', 'public/system', 'public/assets', 'tmp/cache']`

##### `system_dirs`

A hash of directories to be used for symlinking to system directories. Does nothing unless `system_dir_symlinks` is `true`. Default: `{ log_dir => 'log', pid_dir => 'tmp/pids', cache_dir => 'tmp/cache', }`

##### `system_dir_map`

A hash mapping the keys in `system_dirs` to system directories. Does nothing unless `system_dir_symlinks` is `true`. Default: `{ log_dir => '/var/log', pid_dir => '/var/run', cache_dir => '/var/cache', }`

##### `deploy_root`

The root for your deployments. Default: `/var/www`

##### `dir_mode`

The permissions mode you want directories to be given. Default: `a+rx,ug+ws`

##### `system_dir_symlinks`

Whether you want shared log, pid and cache directories to be symlinked to system directories in /var/run, /var/log and /var/cache. Default: `false`

### Defined Types

#### capistrano::deployment

Sets up a directory structure for a capistrano-deployed app. Set the following parameters if you want something other than the default settings, unless you have changed them when instantiating the class above.

##### `user`

Which user you want to own the deployment. The user must exist already.

##### `shared_dirs`

An array of directories you want in your shared directory. Default: `['log', 'tmp', 'public', 'tmp/pids', 'public/system', 'public/assets', 'tmp/cache']`

##### `system_dirs`

A hash of directories to be used for symlinking to system directories. Does nothing unless `system_dir_symlinks` is `true`. Default: `{ log_dir => 'log', pid_dir => 'tmp/pids', cache_dir => 'tmp/cache', }`

##### `system_dir_map`

A hash mapping the keys in `system_dirs` to system directories. Does nothing unless `system_dir_symlinks` is `true`. Default: `{ log_dir => '/var/log', pid_dir => '/var/run', cache_dir => '/var/cache', }`

##### `deploy_root`

The root for your deployment. Default: `/var/www`

##### `dir_mode`

The permissions mode you want directories to be given. Default: `a+rx,ug+ws`

##### `system_dir_symlinks`

Whether you want shared log, pid and cache directories to be symlinked to system directories in /var/run, /var/log and /var/cache. Default: `false`

#### capistrano::shared_file

Adds a shared file to the specified deployment.

##### `app`

The name of the app deployment. Must already exist

##### `content`

The content for the file.

## Limitations

This module has only been tested on Debian-based OSes. YMMV with others.

## Development

Please follow the guidance for contributing on the [Puppet Forge](https://docs.puppet.com/forge/contributing.html)
